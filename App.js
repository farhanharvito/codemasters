/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

// import {
//   Colors,
//   DebugInstructions,
//   Header,
//   LearnMoreLinks,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';

const App = () => {
  const handleLogin = () => {
    alert('Ini Login Button');
  }
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style= {{width:170, height:36, backgroundColor: '#002558', marginTop: -36, borderRadius: 100, justifyContent: 'center'}}>
          <Text style={{fontWeight:'bold', color: 'white', textAlign: 'center'}}>Digital Approval</Text>
        </View>
        <Image source={require('./img.png')} style={styles.image} />
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 24,
          width: 288,
          height: 48,
          borderRadius: 5,
          borderColor: 'gray',
          borderWidth: 1,
          padding: 16,
        }}>
          <Image source={require('./email.png')}/>
          <TextInput
          placeholder='Alamat Email'
        />
        </View>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 24,
          width: 288,
          height: 48,
          borderRadius: 5,
          borderColor: 'gray',
          borderWidth: 1,
          padding: 16,
        }}>
          <Image source={require('./pw.png')}/>
          <TextInput
          placeholder='Password'
          secureTextEntry={true}
          />
        </View>
        <Text style={{alignSelf: 'flex-end', marginBottom: 24, fontStyle: 'italic', color:'#287AE5'}}>Reset Password</Text>
        <TouchableOpacity style={styles.loginButton} onPress={handleLogin}>
          <Text style={styles.buttonText}> LOGIN  </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: '#F7F7F7',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
  },
  content:{
    borderRadius: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    
    padding: 16,
    alignItems: 'center',
  },
  title:{
    fontWeight:'bold',
    fontSize: 16,
    backgroundColor: 'white',
    textAlign: 'center',
  },
  image: {
    alignSelf: 'center',
    marginBottom: 38,
    width: 127,
    height: 55,
  },
  input: {
    width: 288,
    height: 48,
    borderRadius: 5,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 24,

  },
  loginButton: {
    backgroundColor: '#287AE5',
    paddingVertical: 10,
    borderRadius: 5,
    width: 288,
    height: 48, 
  },
  buttonText: {
    alignItems: 'center',
    fontWeight:'bold',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    
  },
});
  export default App;